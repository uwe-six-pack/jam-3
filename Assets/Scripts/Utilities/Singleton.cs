using UnityEngine;

namespace Utilities
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static bool _applicationIsQuitting;
        private static T _instance;
        private static readonly object _lock = new object();

        public static T Instance
        {
            get
            {
                if (_applicationIsQuitting) return null;
                lock (_lock)
                {
                    if (_instance != null) return _instance;
                    _instance = (T) FindObjectOfType(typeof(T));

                    if (FindObjectsOfType(typeof(T)).Length > 1) return _instance;

                    if (_instance != null) return _instance;
                    var singleton = new GameObject();
                    _instance = singleton.AddComponent<T>();
                    singleton.name = "(singleton) " + typeof(T);

                    return _instance;
                }
            }
        }

        public void OnDestroy()
        {
            if (IsDontDestroyOnLoad()) _applicationIsQuitting = true;
        }

        private static bool IsDontDestroyOnLoad()
        {
            if (_instance == null) return false;
            return (_instance.gameObject.hideFlags & HideFlags.DontSave) == HideFlags.DontSave;
        }
    }
}